/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inireader;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author Anna2
 */
public class IniEntry extends DefaultMutableTreeNode{
    
    IniSection is;

    public IniEntry(IniSection is) {
        this.is = is;
    }

    public IniSection getIs() {
        return is;
    }

    @Override
    public String toString() {
        return is.getHeader();
    }
    
    
}
