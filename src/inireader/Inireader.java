/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inireader;

import inifile2.Inifile2BaseVisitor;
import inifile2.Inifile2Lexer;
import inifile2.Inifile2Parser;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

/**
 *
 * @author oam
 */
public class Inireader {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Tree node");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        String fname = "C:\\Users\\Anna2\\Documents\\NetBeansProjects\\iniproject\\dbpart.ini";
        try {
            CharStream input = CharStreams.fromStream(new FileInputStream(fname), Charset.forName("Cp1251"));
            Inifile2Lexer lexer = new Inifile2Lexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            Inifile2Parser parser = new Inifile2Parser(tokens);
            ParseTree tree = parser.inifile();
            Inifile2BaseVisitor eval = new Inifile2BaseVisitor();
            eval.visit(tree);
            eval.getInifile().printSections();
            String secName = "";

            IniEntry root = null;
            IniEntry lev2 = null;
            IniEntry lev3 = null;
            IniEntry current = null;

            /*ПРОХОД ПО ЭЛЕМЕНТАМ СЕКЦИЙ*/
            for (IniSection is : eval.getInifile().getSections()) {
                secName = is.getHeader();
                IniEntry ie = new IniEntry(is);
                if (secName.contains("#") == false) {
                    //листик
                    if (current != null) {
                        current.add(ie);
                    }
                } else {
                    if (secName.contains("#1")) {
                        root = ie;
                        current = ie;
                    } else if (secName.contains("#2")) {
                        lev2 = ie;
                        current = ie;
                        if (root != null) {
                            root.add(ie);
                        }
                    } else if (secName.contains("#3")) {
                        if(lev2!=null){
                            lev2.add(ie);
                        }
                        lev3 = ie;
                        current = ie;
                    }
                }
            }
            TreeModel treeModel = new DefaultTreeModel(root);
            

            //printIniTree(tree, eval);
        } catch (IOException ex) {
            Logger.getLogger(Inireader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   /* public static JTree printIniTree(ParseTree tree, Inifile2BaseVisitor eval) {
        JTree treeNew = null;
        try {
            ArrayList<String> first = new ArrayList<>();
            ArrayList<String> second = new ArrayList<>();
            ArrayList<String> third = new ArrayList<>();
            ArrayList<String> other = new ArrayList<>();
            if (eval != null) {
                ArrayList<IniSection> sections = new ArrayList<>();
                sections = eval.getInifile().getSections();
                if ((sections != null) && (sections.isEmpty() == false)) {
                    for (IniSection is : sections) {
                        //System.out.println("section header: " + is.getHeader());
                        /*Распарсим на массивы для дерева*/
                  /*      if ((is.getHeader() != null) && (is.getHeader().isEmpty() == false)) {
                            String header = is.getHeader();
                            if (header.contains("#")) {
                                if ((header.contains("#1"))) {
                                    String nameNode = header.substring(header.indexOf("#1") + 2, header.length());
                                    first.add(nameNode);
                                    //DefaultMutableTreeNode firstNode = new DefaultMutableTreeNode(nameNode);
                                } else if ((header.contains("#2"))) {
                                    String nameNode = header.substring(header.indexOf("#2") + 2, header.length());
                                    second.add(nameNode);
                                } else if ((header.contains("#3"))) {
                                    String nameNode = header.substring(header.indexOf("#3") + 2, header.length());
                                    third.add(nameNode);
                                }
                            } else {
                                other.add(header);
                            }
                        }
                    }
                    System.out.println("first ->");
                    for (String s : first) {
                        System.out.println(s);
                    }
                    System.out.println("second ->");
                    for (String s : second) {
                        System.out.println(s);
                    }
                    System.out.println("third->");
                    for (String s : third) {
                        System.out.println(s);
                    }
                } else {
                    System.out.println("sections:" + sections);
                }

                /*попробуем построить дерево*/
             /*   DefaultMutableTreeNode node1 = null;
                DefaultMutableTreeNode node2 = null;
                DefaultMutableTreeNode node3 = null;
                for (String s : first) {
                    node1 = new DefaultMutableTreeNode(s);
                }
                for (int i = 0; i < second.size(); i++) {
                    if (second.get(i).matches("[0-9]'.'(~)")) {
                        node2 = new DefaultMutableTreeNode(second.get(i));
                        for (int j = 0; j < third.size(); j++) {
                            if (third.get(j).matches("'" + (i + 1) + "' " + "'.' [0-9]")) {
                                node3 = new DefaultMutableTreeNode(third.get(j));
                                node2.add(node3);
                            }
                        }
                        node1.add(node2);
                    }
                }

                treeNew = new JTree(node1);

            } else {
                System.out.println("eval is null");
            }
        } catch (Exception e) {
            Logger.getLogger(Inireader.class.getName()).log(Level.SEVERE, null, e);
        }
        return treeNew;
    }*/

}
