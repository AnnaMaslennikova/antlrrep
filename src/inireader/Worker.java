/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inireader;

import inifile2.Inifile2BaseVisitor;
import inifile2.Inifile2Lexer;
import inifile2.Inifile2Parser;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Anna2
 */
public class Worker {
    
    
    
    public /*JTree*/ TreeModel printIniTree(ParseTree tree, Inifile2BaseVisitor eval) {
      //  JTree treeNew = null;
        TreeModel model = null;
        try {
            ArrayList<String> first = new ArrayList<>();
            ArrayList<String> second = new ArrayList<>();
            ArrayList<String> third = new ArrayList<>();
            ArrayList<String> other = new ArrayList<>();
            if (eval != null) {
                ArrayList<IniSection> sections = new ArrayList<>();
                sections = eval.getInifile().getSections();
                if ((sections != null) && (sections.isEmpty() == false)) {
                    for (IniSection is : sections) {
                        //System.out.println("section header: " + is.getHeader());
                        /*Распарсим на массивы для дерева*/
                        if ((is.getHeader() != null) && (is.getHeader().isEmpty() == false)) {
                            String header = is.getHeader();
                            if (header.contains("#")) {
                                if ((header.contains("#1"))) {
                                    String nameNode = header.substring(header.indexOf("#1") + 2, header.length());
                                    first.add(nameNode);
                                    //DefaultMutableTreeNode firstNode = new DefaultMutableTreeNode(nameNode);
                                } else if ((header.contains("#2"))) {
                                    String nameNode = header.substring(header.indexOf("#2") + 2, header.length());
                                    second.add(nameNode);
                                } else if ((header.contains("#3"))) {
                                    String nameNode = header.substring(header.indexOf("#3") + 2, header.length());
                                    third.add(nameNode);
                                }
                            } else {
                                other.add(header);
                            }
                        }
                    }
                    System.out.println("first ->");
                    for (String s : first) {
                        System.out.println(s);
                    }
                    System.out.println("second ->");
                    for (String s : second) {
                        System.out.println(s);
                    }
                    System.out.println("third->");
                    for (String s : third) {
                        System.out.println(s);
                    }
                } else {
                    System.out.println("sections:" + sections);
                }

                /*попробуем построить дерево*/
                DefaultMutableTreeNode root = null;
                DefaultMutableTreeNode node1 = null; //#1
                DefaultMutableTreeNode node2 = null;  //#2
                DefaultMutableTreeNode node3 = null;   //#3
                DefaultMutableTreeNode node4 = null;   //
                for (String s : first) {
                    node1 = new DefaultMutableTreeNode(s);
                }
                for (int i = 0; i < second.size(); i++) {
                    if (StringUtils.countMatches(second.get(i), (i+1)+".") == 1) {
                        //System.out.println("i="+i+", node = "+second.get(i));
                        node2 = new DefaultMutableTreeNode(second.get(i));
                        for (int j = 0; j < third.size(); j++) {
                         //   System.out.println("third["+j+"]="+third.get(j));
                            String regexj = (i+1)+"."+(j+1)+". ";
                            //System.out.println("regexj="+regexj);
                            if((StringUtils.countMatches(third.get(j), regexj) == 1)||(StringUtils.countMatches(other.get(j), regexj)==1)){
                             //   System.out.println("j!!!!");
                                node3 = new DefaultMutableTreeNode(third.get(j));
                                
                                for(int k=0; k<other.size(); k++){
                                    if(StringUtils.countMatches(other.get(k), (i+1)+"."+(j+1)+"."+(k+1)+".")==1){
                                        node4 = new DefaultMutableTreeNode(other.get(k));
                                        node3.add(node4);
                                    }else{
                                        System.out.println("in k cicle there is no matches");
                                    }
                                }
                                node2.add(node3);
                            }else{
                                System.out.println("in j cicle there is no matches");
                            }
                        }
                        node1.add(node2);
                    }else{
                        System.out.println("не .matches(\"[0-9]'.'(~)\")");
                    }
                }
//                for(String kt: other){
//                    System.out.println("kt:"+kt);
//                }
               // treeNew = new JTree(node1);
//              for(int i=0; i<other.size(); i++){
//                   
//               }
                model = new DefaultTreeModel(node1);
            } else {
                System.out.println("eval is null");
            }
        } catch (Exception e) {
            Logger.getLogger(Inireader.class.getName()).log(Level.SEVERE, null, e);
        }
       // return treeNew;
       return model;
    }

    public TreeModel showTree(String fname){
        TreeModel treeModel = null;
        try {
            CharStream input = CharStreams.fromStream(new FileInputStream(fname), Charset.forName("Cp1251"));
            Inifile2Lexer lexer = new Inifile2Lexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            Inifile2Parser parser = new Inifile2Parser(tokens);
            ParseTree tree = parser.inifile();
            Inifile2BaseVisitor eval = new Inifile2BaseVisitor();
            eval.visit(tree);
            eval.getInifile().printSections();
            String secName = "";
            IniEntry root = null;
            IniEntry lev2 = null;
            IniEntry lev3 = null;
            IniEntry current = null;
            /*ПРОХОД ПО ЭЛЕМЕНТАМ СЕКЦИЙ*/
            for (IniSection is : eval.getInifile().getSections()) {
                secName = is.getHeader();
                IniEntry ie = new IniEntry(is);
                if (secName.contains("#") == false) {
                    //листик
                    if (current != null) {
                        current.add(ie);
                    }
                } else {
                    if (secName.contains("#1")) {
                        ie.is.setHeader(is.getHeader().substring(secName.indexOf("#1")+1, secName.length()));
                        root = ie;
                        current = ie;
                    } else if (secName.contains("#2")) {
                        ie.is.setHeader(is.getHeader().substring(secName.indexOf("#2")+1, secName.length()));
                        lev2 = ie;
                        current = ie;
                        if (root != null) {
                            root.add(ie);
                        }
                    } else if (secName.contains("#3")) {
                        ie.is.setHeader(is.getHeader().substring(secName.indexOf("#3")+1, secName.length()));
                        if(lev2!=null){
                            lev2.add(ie);
                        }
                        lev3 = ie;
                        current = ie;
                    }
                }
            }
            treeModel = new DefaultTreeModel(root);
        } catch (IOException ex) {
            Logger.getLogger(Inireader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return treeModel;
    }
    
    public String receiveFilePath(Object selectedLeaf){
        String filepath = "";
        System.out.println("Start receiveFilePath..");
        String key; String value;
        try {
            IniEntry leaf = (IniEntry)selectedLeaf;
           // if(leaf.is..getChildren().size()>0){
                Map<String, String> keyval = leaf.getIs().getKeyval();
                for (Map.Entry<String, String> entry : keyval.entrySet()) {
                    key = entry.getKey();
                    if (key.equalsIgnoreCase("PATHDAT")) {
                        System.out.println("PathDat is found!");
                        value = entry.getValue();
                        filepath = value;
                    }
                }
           // }else{
               // filepath = "";
                        //"Файл с данными по выбранному элементу отсутствует..";
           // }
            
        } catch (Exception e) {
            Logger.getLogger(Inireader.class.getName()).log(Level.SEVERE, null, e);
        }
        System.out.println("filepath = "+filepath);
        return filepath;
    }
    
}
